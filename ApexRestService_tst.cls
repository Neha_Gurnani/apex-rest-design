@isTest
private class ApexRestService_tst {
    @isTest
    private static void testGetAccount(){
        TestData.createAccount();
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Account';
        request.httpMethod = 'GET';
        RestContext.request = request;
        List<Account> accList = ApexRestService.getAccount();
        
        request.requestURI = '/services/apexrest/Account';
        request.addParameter('name', 'test');
        request.addParameter('Limit', '10');
        request.addParameter('sortField', 'name');
        request.httpMethod = 'GET';
        RestContext.request = request;
        accList = ApexRestService.getAccount();
        
        System.assert(accList.size()>0);
    }
    
    @isTest
    private static void testPostAccount(){
        Account accRec = new Account();
        accRec.name = 'Test';
        accRec.Website = 'test@test.com';
        String accJson = Json.serialize(accRec);
        
		RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Account';
        request.httpMethod = 'POST';
        request.addParameter('name', 'test');
        request.requestBody = Blob.valueOf('['+accJson+']');
        
        RestContext.request = request;
        List<Account> accList = ApexRestService.postAccount();

        System.assertEquals('Test', accList[0].name);
    }
    
    @isTest
    private static void testUpdateAccount(){
        Account accRec = TestData.createAccount();
        
        accRec.Name = 'test1';

        String accJson = Json.serialize(accRec);
		RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Account';
        request.httpMethod = 'PUT';
        request.requestBody = Blob.valueOf('['+accJson+']');
        RestContext.request = request;
        List<Account> accList = ApexRestService.updateAccount();
        
        System.assertEquals('test1', accList[0].name);
    }
    
    @isTest
    private Static void testDeleteAccount(){
        Account accRec = new Account();
        accRec.name = 'Test';
        accRec.Website = 'test@test.com';
        insert accRec;
        
        RestRequest request = new RestRequest();
        request.requestURI = '/services/apexrest/Account';
        request.addParameter('name', accRec.id);
        RestContext.request = request;
         ApexRestService.deleteAccount();
    }
    
}