@RestResource(urlMapping='/Account')
global class ApexRestService {
    
    @HTTPGet
    global static List<Account> getAccount(){
         Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String query=getData();
        String validation;
        if(req.params.keySet().contains('sortField')){
            validation= req.params.get('sortField');
            if(schemaFieldMap.keySet().contains(validation)){
                query+=' ORDER BY '+validation;
            }
        }
         if(req.params.keySet().contains('Limit')){
            validation = req.params.get('Limit');
            query+=' LIMIT '+validation;
        }
        /*
         *  Fetch data according to last modified 10 days*/
        Date previousDate = System.Today()-10;
        query+=query.contains('WHERE') ?'' : ' WHERE Day_only(lastModifiedDate) >: previousDate ';
        System.debug('query'+query);	
		
        List<Account> result = Database.query(query);
        return result;
    }
    
    @HTTPPost
    global static List<Account> postAccount(){
        List<Account> accList=retrieveData();
        insert accList;
        return accList;
    }
    
    @HTTPDelete
    global static void deleteAccount(){
        Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        String filter;
        String validation;
        String query='SELECT Id, Name, Phone, Website FROM Account';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqUri=req.requestURI;
        for(String param:req.params.keySet()){
            if(schemaFieldMap.keySet().contains(param)){
                filter=req.params.get(param);
                system.debug('filter'+filter);
                query+=(query.contains('WHERE'))?' and '+param+' = \'' +filter +'\'' : ' WHERE '+param+' = \''+  filter +'\'';
            }
        }
        if(req.params.keySet().contains('q')){
            validation=req.params.get('q');
            query=validation;
        }

        List<Account> result = Database.query(query);
        delete result;
    }
    @HTTPPut
    global static List<Account> updateAccount(){
        List<Account> accList=retrieveData();
        update accList;
        return accList;
    }
    public static List<Account> retrieveData(){
		RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String jsonString=req.requestBody.toString();
        List<Account> accList=(List<Account>)Json.deserialize(jsonString, List<Account>.class);
        return accList;
    }
    public static String getData(){
         Map<String, Schema.SObjectField> schemaFieldMap = Schema.SObjectType.Account.fields.getMap();
        String filter;
        String validation;
        String query='SELECT Id, Name, Phone, Website FROM Account ';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String reqUri=req.requestURI;
        for(String param:req.params.keySet()){
            if(schemaFieldMap.keySet().contains(param)){
                filter=req.params.get(param);
                system.debug('filter'+filter);
                query+=(query.contains('WHERE'))?' and '+param+' = \'' +filter +'\'' : ' WHERE '+param+' = \''+  filter +'\'';
            }
        }
        if(req.params.keySet().contains('q')){
            validation=req.params.get('q');
            query=validation;
        }
        return query;
    }
    
}